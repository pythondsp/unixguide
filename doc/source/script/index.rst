Shell scripting 
===============

In this section, Unix-shell-scripting is discussed. 

Hello World
-----------

Files containing the shell scripts are usually saved as filename.sh. 

.. code-block:: shell

    # helloworld.sh

    echo "Hello World"
    echo Hello World Again

The 'bash' or 'sh' command can be used to execute the above file. Further, we can change the permission of the file using 'chmod' command and execute it directly as shown below,  

.. code-block:: shell

    $ bash helloworld.sh
    Hello World
    Hello World Again

    $ sh helloworld.sh
    Hello World
    Hello World Again

    $ chmod 777 helloworld.sh
    $ ./helloworld.sh 
    Hello World
    Hello World Again


Execute commands from script
----------------------------

We can store various commands in a file and then run all those commands by executing the file. This is quite useful for repetitive tasks, 

.. code-block:: shell

    # run_commands.sh

    echo 'list of files'
    ls

    echo 'date'
    date

.. code-block:: shell

    $ sh run_commands.sh 
    list of files
    helloworld.sh  run_commands.sh	testfile.txt
    date
    Mon Feb 27 07:10:33 NZDT 2017

Shell Variables and input from user
-----------------------------------

Shell variable
^^^^^^^^^^^^^^

In below code, 'n' is the variable; and $ sign is used to display the value of the variable, 

.. code-block:: shell

    # test.sh

    # no spaces between variable and input
    n='Tiger'

    echo $n

Output is shown below, 

.. code-block:: shell

    $ echo test.sh
    Tiger

Arithmetic
^^^^^^^^^^

For mathematical operation, **$((expression))** is used as shown in following example, 

.. code-block:: shell
    
    # test.sh

    i=1

    j=$((i+1))

    echo $j


Output of above code is shown below, 

.. code-block:: shell

    $ bash test.sh 
    2

Single quote vs double quote
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Note the differences between the outputs with single and double quote, 

.. code-block:: shell
    
    # test.sh

    i=1

    j=$((i+1))

    echo '$j'
    echo "$j"

Below are the outputs, 

.. code-block:: shell

    
    $ bash test.sh 
    $j
    2

User input
^^^^^^^^^^

'read' is used to get input from user, 

.. code-block:: shell

       
    # run_commands.sh

    echo enter your name
    read name

    echo Hello $name, How are you?
    
.. code-block:: shell

    $ bash run_commands.sh 
    enter your name
    Meher
    Hello Meher, How are you?



Positional arguments
--------------------

We can read some arguments while executing the code as well. $1 and $2 are the positional arguments provided by the user, 

.. code-block:: shell

    # test.sh

    # $1 and $2 are the arguments provided while executing the file
    echo $1 $2

.. code-block:: shell

    $ bash test.sh 12
    12
    $ bash test.sh 12 13
    12 13
    $ bash test.sh 12 13 14
    12 13

* 'set' command can used to convert an string to positional parameter as shown below,

.. code-block:: shell

    $ set Meher Krishna Patel
    $ echo $1
    Meher
    $ echo $2
    Krishna
    $ echo $3
    Patel
    $ echo $4


* (` \`) can be used to save the outputs of the command as positional parameter, 

.. code-block:: shell
    
    $ cat > test
    How are you?
    ^C
    $ cat test
    How are you?
    $ set `cat test`
    $ echo $1
    How
    $ echo $2
    are


Mathematical operations
-----------------------

'expr', $, $((expression)) or ' | bc ' commands can be used for mathematical operation as shown below. 


.. note:: There is no spaces between x=3. 


.. code-block:: shell

    # test.sh

    x=4
    y=2


    echo $x + $y
    echo `expr $x + $y`   # ` ` is required with expr for maths operations

    i=$(expr $x + 1)
    echo $i

    z=`echo $x \* $y | bc`   # * has special meaning, hence \*
    echo $z
    
    d=$(($x / $y))  # using double bracket $((expression))
    echo $d
      
if-else
-------

Note that [ ] are used for condition-check operations (not required for other cases). Also, look for the spaces after [ and before ]. 

.. code-block:: shell

    # test.sh

    echo enter a number 
    read x

    if [ $x -gt 4 ]
    then
        echo 'greater than 4'
    elif [ $x -eq 4 ]
    then
        echo 'equal to 4'
    else 
        echo 'less then 4'
    fi

Following is the output of above code, 

.. code-block:: shell

    
    $ sh test.sh 
    enter a number
    6
    greater than 4


* Below code check whether the file exists in the directory or not. Similarly '-d', '-r' and '-w' can be used for checking the directory, read and write permission respectively

.. code-block:: shell

    # test.sh

    echo enter a filename
    read x

    if [ -f $x ]
    then
        echo 'yes, there is file' 
    else 
        echo 'no, there is no such file'
    fi

Following is the output, 

.. code-block:: shell
    
    $ sh test.sh 
    enter a filename
    test.sh
    yes, there is file


Case statement
--------------

Use double quote with case statement, 

.. code-block:: shell

    # test.sh

    # case '$1'   # do not use single quote
    case "$1"
    in
        0) echo zero;;
        1) echo one;;
        *) echo 'neither 0 nor 1';;
    esac

Following are the outputs, 
  
.. code-block:: shell
    
    $ bash test.sh 1
    one
    $ bash test.sh 0
    zero
    $ bash test.sh 3
    neither 0 nor 1


For loop
--------

* Below code loop over all the files of the directory and print the result, 

.. code-block:: shell

    # test.sh

    for files in *
    do
        echo $files
    done

Following is the output of above code, which listed all the files in the current directory, 

.. code-block:: shell
    
    $ bash test.sh
    helloworld.sh
    meher.txt
    month.txt
    m.txt
    run_commands.sh
    surname.txt
    temp.txt
    test.sh

* Following is the another example,  

.. code-block:: shell

    
    # test.sh

    for files in 1 2 3 
    do
        echo $files
    done

    # outputs
    # $ bash test.sh
    # 1
    # 2
    # 3

While loop
----------

Following are the example of while loop, 

.. code-block:: shell

    # test.sh

    i=1

    while [ "$i" -lt 4 ]
    do
        echo $i
        i=$(($i+1))
    done


    # outputs
    # $ bash test.sh
    # 1
    # 2
    # 3

