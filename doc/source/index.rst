.. Unix commands cheatsheet documentation master file, created by
   sphinx-quickstart on Thu Feb 23 10:55:43 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Unix Guide 
==========


.. toctree::
   :maxdepth: 3
   :numbered:
   :includehidden:
   :caption: Contents:

   unixcheatsheet/index
   script/index


.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`

